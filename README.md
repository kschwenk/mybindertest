# Visualizing the Barn-Pole Paradox

This is just a little test to see if I can get a Jupyter notebook running over at MyBinder.
Strange Badge thingy:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/kschwenk%2Fmybindertest/HEAD?labpath=barnpole.ipynb)


## License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/)

[CC0 1.0 Universal public domain dedication](CC0 1.0 Universal public domain dedication):
To the extent possible under law, the authors have waived all copyright and related or neighboring rights to this project.
This work is published from: Germany.
