using SonOfAnton
using Tracy
using Images
using StaticArrays

Box = AxisAlignedParametricBox

function barnpole(filenamebase,
    polespd,
    barndoorspd,
    polelen)

    v = Vec3d(polespd, 0, 0)
    u = Vec3d(0, barndoorspd, 0)
    l = polelen

    # We make a perspective image, an orthographic image, and a spacetime
    # diagram each for the barn frame and the pole frame
    barnpersimg = zeros(Col3d, (300, 400, 40))
    polepersimg = zeros(Col3d, (300, 400, 40))
    barnparaimg = zeros(Col3d, (400, 400, 40))
    poleparaimg = zeros(Col3d, (400, 400, 40))
    barnstdimg = zeros(Col3d, (400, 400, 1))
    polestdimg = zeros(Col3d, (400, 400, 1))

    # barntex = UniformTexture(Col3d(1,0,0))
    # doortex = UniformTexture(Col3d(0,1,0))
    # poletex = UniformTexture(Col3d(0,0,1))
    barntex = ProceduralTexture(x -> checkerboard(x, Col3d(1,0,0), Col3d(0,1,1)), :point)
    doortex = ProceduralTexture(x -> checkerboard(x, Col3d(0,1,0), Col3d(1,0,1)), :point)
    poletex = ProceduralTexture(x -> checkerboard(x, Col3d(0,0,1), Col3d(1,1,0)), :point)
    barnmat = TextureMaterial(barntex)
    doormat = TextureMaterial(doortex)
    polemat = TextureMaterial(poletex)

    scene = Frame(rest4d(),
    [
        # barn
        Frame(rest4d(),
        [
            Surface(Box((Vec3d(-5.0, -5.1, -1.0), Vec3d(5.0, 5.1, -1.1))), barnmat),
            Surface(Box((Vec3d(-5.0, 5.0, -1.0), Vec3d(5.0, 5.1, 1.0))), barnmat),
            Surface(Box((Vec3d(-5.0, -5.1, -1.0), Vec3d(5.0, -5.0, 1.0))), barnmat),
            Frame(translate4d(0, 0, 20, 0),
            [
                PerspectiveCamera(deg2rad(60), 40, barnpersimg),
                ParallelCamera(20 * Vec4d(-1/2, -1/2, 0, -1/2),
                               SA[20 * Vec4d(1, 0, 0, 0),
                                  20 * Vec4d(0, 1, 0, 0),
                                  40 * Vec4d(0, 0, -1, 0)],
                               40,
                               barnparaimg),
                ParallelCamera(20 * Vec4d(-1/2, 0, 0, -1/2),
                               SA[20 * Vec4d(1, 0, 0, 0),
                                  20 * Vec4d(0, 0, 0, 1),
                                  40 * Vec4d(0, 0, -1, 0)],
                               0.0,
                               barnstdimg)
            ])
        ]),
        # barn doors
        Frame(boost31d(u),
        [
            Surface(Box((Vec3d(-6.0, -5.1, -1.1), Vec3d(-5.0, 5.1, 1.1))), doormat)
            Surface(Box((Vec3d(5.0, -5.1, -1.1), Vec3d(6.0, 5.1, 1.1))), doormat)
        ]),
        # pole
        Frame(boost31d(v),
        [
            Surface(Box((Vec3d(-l/2, -1/2, -1/2), Vec3d(l/2, 1/2, 1/2))), polemat),
            Frame(translate4d(0, 0, 20, 0),
            [
                PerspectiveCamera(deg2rad(60), 40, polepersimg),
                ParallelCamera(20 * Vec4d(-1/2, -1/2, 0, -1/2),
                               SA[20 * Vec4d(1, 0, 0, 0),
                                  20 * Vec4d(0, 1, 0, 0),
                                  40 * Vec4d(0, 0, -1, 0)],
                               40,
                               poleparaimg),
                ParallelCamera(40 * Vec4d(-1/2, 0, 0, -1/2),
                               SA[40 * Vec4d(1, 0, 0, 0),
                                  40 * Vec4d(0, 0, 0, 1),
                                  40 * Vec4d(0, 0, -1, 0)],
                               0.0,
                               polestdimg)
            ])
        ])
    ])

    render!(scene)

    save(filenamebase * "-barnpers.gif", barnpersimg)
    save(filenamebase * "-barnpara.gif", barnparaimg)
    save(filenamebase * "-barnstd.gif", barnstdimg)
    save(filenamebase * "-polepers.gif", polepersimg)
    save(filenamebase * "-polepara.gif", poleparaimg)
    save(filenamebase * "-polestd.gif", polestdimg)

    return
end

polespd = 0.9
barndoorspd = 0.99
polelen = 15.0

barnpole("barnpole-statpole-statdoors", 0*polespd, 0*barndoorspd, polelen)
barnpole("barnpole-movpole-statdoors", 1*polespd, 0*barndoorspd, polelen)
barnpole("barnpole-movpole-movdoors", 1*polespd, 1*barndoorspd, polelen)
